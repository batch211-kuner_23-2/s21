//console.log("Hello World!");

let studentNumberA = '2020-1923';
let studentNumberB = '2020-1924';
let studentNumberC = '2020-1925';
let studentNumberD = '2020-1926';
let studentNumberE = '2020-1927';

let studentNumbers = ['2020-1923','2020-1924','2020-1925','2020-1926','2020-1927'];

// Arrays

let grades = [98.5,94.3,89.2,90.1];
let computerBrands = ['acer','asus','lenovo','neo','redfox','gateway','toshiba','fujitsu'];
console.log(grades);
console.log(computerBrands);

//alternative way to write arrays

let myTask = [
	'drink html',
	'eat javascript',
	'inhale css',
	'bake sass'
]
console.log(myTask);

// .length property

console.log(myTask.length); //4

let blankArr =[];
console.log(blankArr.length); //0


let fullName = 'Janie Noble';
console.log(fullName.length);

myTask.length =	myTask.length-1; // minus 1 sa loob ng array sa pinakahuli
console.log(myTask.length); // 3
console.log(myTask); // 'drink html', 'eat javascript', 'inhale css'

//another example using decrementation


fullName.length = fullName.length-1;
console.log(fullName.length);
fullName.length--;
console.log(fullName);

let theBeatles = ['John','Paul','Ringo','George'];
//theBeatles.length++;
theBeatles[4] = 'Cardo';
theBeatles[theBeatles.length] = 'Ely';
theBeatles[theBeatles.length] = 'Chito';
theBeatles[theBeatles.length] = 'MJ';
console.log(theBeatles);

// let theTrainer = ['Ash'];

// theTrainer[theTrainer.length] = 'Brock';
// theTrainer[theTrainer.length] = 'Misty';

// console.log(theTrainer);

// function addTrainers(trainer) {
// 	theTrainers.[theTrainers.length] = trainer;
// }
// addTrainers('Misty');
// addTrainers('Brock');
// console.log(theTrainers);

console.log(grades[0]); // 98.5
console.log(computerBrands[3]); // neo

console.log(grades[20]); // undefined

let lakersLegends = ['Kobe','Shaq','Lebron','Magic','Kareon'];
console.log(lakersLegends[1]);
console.log(lakersLegends[3]);

let currentLaker = lakersLegends[2]; //store to another variable
console.log(currentLaker);
console.log(currentLaker);

console.log("Array before reassignment");
console.log(lakersLegends);
lakersLegends[2] = 'Pau Gasol';
console.log("Array after reassignment");
console.log(lakersLegends);


function findBlackMamba(index) {
	return lakersLegends[index]
}
let blackMamba = findBlackMamba(0);
console.log(blackMamba);

let favoriteFoods = [
	'Tonkatsu',
	'Adobo',
	'Hambuger',
	'Sinigang',
	'Pizza'
];
console.log(favoriteFoods);

favoriteFoods[3] = 'Siomai';
favoriteFoods[4] = 'Takoyaki';

// Accessing the last element of an array

let bullsLegends = ['Jordan','Pippen','Rodman','Rose','Kakoc'];

let lastElementIndex = bullsLegends.length-1;
console.log(lastElementIndex); // 4
console.log(bullsLegends.length); // 5

console.log(bullsLegends[lastElementIndex]); //
console.log(bullsLegends[bullsLegends.length-1]);

// adding items into the array
let newArr = [];
console.log(newArr[0]);

newArr[0] = 'Cloud Strife';
console.log(newArr);

console.log(newArr[1]); //undefined
newArr[1] = "Tifa Lockhart";
console.log(newArr);

//newArr[newArr.length-1] = 'Aertith Gainsborough'; // mawala c Tifa

newArr[newArr.length] = 'Barret Wallace'; // ma add c barret
console.log(newArr);

// Looping over an array

for (let index = 0; index < newArr.length; index++) {
	console.log(newArr[index]);
}


let numArr = [5,12,30,46,40];

for (let index = 0; index < numArr.length; index++) {

	if (numArr[index] % 5 === 0) {
		console.log(numArr[index] + ' is divisbile by 5');
	} else {
		console.log(numArr[index] + " is not divisbile by 5");
	}
}


// Multi-dimensional Array
let chessBoard = [

	['a1','b1','c1','d1','e1','f1','g1','h1'],
	['a2','b2','c2','d2','e2','f2','g2','h2'],
	['a3','b3','c3','d3','e3','f3','g3','h3'],
	['a4','b4','c4','d4','e4','f4','g4','h4'],
	['a5','b5','c5','d5','e5','f5','g5','h5'],
	['a6','b6','c6','d6','e6','f6','g6','h6'],
	['a7','b7','c7','d7','e7','f7','g7','h7'],
	['a8','b8','c8','d8','e8','f8','g8','h8']
];
console.log(chessBoard);

console.log(chessBoard[1][4]);
console.log("Pown moves to: " + chessBoard[1][5]); // f2

console.log(chessBoard[7][0]);
console.log(chessBoard[5][7]);

